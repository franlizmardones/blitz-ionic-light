import { ErrorBoundary, ErrorComponent, useQueryErrorResetBoundary } from "blitz"

// Ionic imports
import { IonApp, setupIonicReact } from "@ionic/react"
import "@ionic/react/css/core.css"
import "@ionic/react/css/normalize.css"
import "@ionic/react/css/structure.css"
import "@ionic/react/css/typography.css"
import "@ionic/react/css/padding.css"
import "@ionic/react/css/float-elements.css"
import "@ionic/react/css/text-alignment.css"
import "@ionic/react/css/text-transformation.css"
import "@ionic/react/css/flex-utils.css"
import "@ionic/react/css/display.css"

// Ionic setup
setupIonicReact({
  mode: "md",
})

export default function App({ Component, pageProps }) {
  const getLayout = Component.getLayout || ((page) => page)

  return (
    <ErrorBoundary
      FallbackComponent={RootErrorFallback}
      onReset={useQueryErrorResetBoundary().reset}
    >
      {getLayout(
        <IonApp>
          <Component {...pageProps} />
        </IonApp>
      )}
    </ErrorBoundary>
  )
}

function RootErrorFallback({ error }) {
  return <ErrorComponent statusCode={error.statusCode || 400} title={error.message || error.name} />
}

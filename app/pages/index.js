import { dynamic } from "blitz"

const DynamicLogin = dynamic(() => import("../components/login"), {
  ssr: false,
})

const Index = () => {
  return <DynamicLogin />
}

export default Index
